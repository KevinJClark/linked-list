//File: 		ListClass.cpp
//Name: 		Kevin Clark
//UserID:		cs301117
//Class:		CSCI301 section 1
//Due:			02/15/2018
//Project: 		#5
//Purpose:		Defines the contents of member functions.
//				This file holds the class code.
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "ListClass.h"

//Private:

List::Node* List::NewNode(ItemType ItemValue, Node* NextPtr)
{//Create a new node and return its memory location to be used
 //as a pointer in other functions.
	Node* NodeMemLocation; 
	NodeMemLocation = new Node;
	NodeMemLocation -> Item = ItemValue;
	NodeMemLocation -> Next = NextPtr;
	return NodeMemLocation;
}

//Public:

void List::operator=(List Source)
{
	List::Node* Traveler;	// Will traverse the source List.
	List::Node* Last;		// Will always point to the new List's last Node.

	if (Source.First == NULL)   // If the source list is empty...
	{
		First = NULL; //This is what an empty list looks like
	}
	else
	{
		First = NewNode(Source.First -> Item, NULL); //Copy the first Node.
		Last = First;
		Traveler = Source.First -> Next;
		while (Traveler != NULL) //Copy remaining Nodes.
		{
			Last -> Next = NewNode(Traveler -> Item, NULL);
			Last = Last -> Next;
			Traveler = Traveler -> Next;
		}
	}
}

void List::EmptyList()
{
	Node* Traveler;
	while(First != NULL)
	{
		Traveler = First;
		First = First -> Next;
		delete Traveler;
	}
}
	
bool List::IsPresent(ItemType SearchItem) const
{
	Node* Pointer;
	for(Pointer = First; //One big, gross, for loop
	Pointer != NULL && Pointer -> Item != SearchItem;
	Pointer = Pointer -> Next);
			
	return (Pointer != NULL);
}

bool List::IsEmpty() const
{
	if(First == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void List::Insert(ItemType Entry)
{
	if(IsPresent(Entry)) //Entry is already in the list.
	{					 //No need to put it in again.
		return;
	}
	Node *Previous;
	
	//if list is empty or if new entry is smaller than first item
	if(First == NULL || Entry < First -> Item)
	{
		First = NewNode(Entry, First);
	}
	else
	{
		Previous = First;
		while(Previous -> Next != NULL && Previous -> Next -> Item < Entry)
		{//while our "Previous" pointer hasn't reached end of the array
		 //AND the next item is smaller than our entry item...
			Previous = Previous -> Next; //Increment the "Previous" pointer
		}
		Previous -> Next = NewNode(Entry, Previous -> Next);
	}
}

void List::Delete(ItemType Target)
{
	Node *Temp;
	Node *Previous;

	if(!IsPresent(Target)) //Target is not in the list
	{					   //so perform no action
		return; 
	}

	Previous = First;
	if(Previous -> Item == Target)
	{
		First = First -> Next;
		delete Previous;
	}
	else
	{
		while(Previous -> Next != NULL && Previous -> Next -> Item < Target)
		Previous = Previous -> Next;
		Temp = Previous -> Next;
		Previous -> Next = Temp -> Next;
		delete Temp;
	}
}

int List::GetLength() const
{//Counts the number of nodes and stops when last node encountered
 //(Pointer == NULL).  Increments counter value once for each node,
 //and returns the counter value at last node.
	Node* Pointer;
	int Counter= 0;

	for (Pointer = First; Pointer != NULL; Pointer = Pointer -> Next)
	{
		Counter++;
	}
	return Counter;
}

void List::Merge(List List1, List List2) //Merges two lists
{
	//Precondition: Output list should be empty.
	//Both lists must be sorted from low to high.  The insert operator
	//Does this for us
	Node* L1Pointer; //Traveling pointer for input list 1
	Node* L2Pointer; //Traveling pointer for input list 2
	L1Pointer = List1.First; //Set both pointers to First
	L2Pointer = List2.First;
	
	
	//While neither list has been exhausted...
	while(L2Pointer != NULL && L1Pointer != NULL)
	{
		//if L1's item is smaller,
		if(L2Pointer -> Item > L1Pointer -> Item)
		{
			//Put L1's item in the output list
			Insert(L1Pointer -> Item);
			L1Pointer = L1Pointer -> Next;
			//and advance the pointer onto the next item in the list
		}
		//All cases (>, <, =) need to be accounted for
		//Note the >= as opposed to the > used in above if statement
		else if(L1Pointer -> Item >= L2Pointer -> Item)
		{
			Insert(L2Pointer -> Item);
			L2Pointer = L2Pointer -> Next;
		}
	}
	if(L2Pointer == NULL) //List 2 is exhausted
	{
		while(L1Pointer != NULL)
		{//while List1 is not exhausted...
			//Put items into the output list
			Insert(L1Pointer -> Item);
			L1Pointer = L1Pointer -> Next;
			//Advance the pointer to the next node from list1
		}
	}
	else if(L1Pointer == NULL) 
	{//Same as above but in the case for List1 being exhausted first
		while(L2Pointer != NULL)
		{
			Insert(L2Pointer -> Item);
			L2Pointer = L2Pointer -> Next;
		}
	}
} //End merge function

List::ItemType List::GetNthNodeItem(int NodeNumber) const
{
	Node *Cursor;

	if(NodeNumber > GetLength() || NodeNumber < 1)
	{//if requested node is greater than list length or negative (or 0)...
		return -1; //Sloppy, but -1 indicates not found
	}

	Cursor = First;
	for(int Counter = 1; Counter < NodeNumber; Counter++)
	{
		Cursor = Cursor -> Next;
	}
	return Cursor -> Item;
}

//Friend function.  Overloading the '<<' operator.
ostream& operator<<(ostream& Output, List& OutList)
{
	List::Node* Cursor;
	
	for(Cursor = OutList.First;					//One
	Cursor != NULL && Cursor -> Next != NULL;	//big
	Cursor = Cursor -> Next)					//for loop.
	{
		Output << Cursor -> Item;
		Output << " ";
	}
	if(Cursor != NULL) //Handles special case of last item, and
	{
		Output << Cursor -> Item; //Outputs the very last item
	}
	return Output; //Return the output stream for the next usage of '<<'
}