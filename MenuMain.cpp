//File: 		MenuMain.cpp
//Name: 		Kevin Clark
//UserID:		cs301117
//Class:		CSCI301 section 1
//Due:			02/15/2018
//Project: 		#5
//Purpose:		Main program that is in charge of user input, menu display,
//				and calling list class functions

#include <iostream>
#include "ListClass.h"
using namespace std;

void DisplayMenu() //Menu function.  Displays user instructions
{
    cout << "e   -- Empty the list" << endl
		 << "i v -- Insert the value v into the list" << endl
		 << "d v -- Delete the node with value v from the list" << endl
		 << "m   -- Show if the list empty" << endl
		 << "l   -- Show the length of the list" << endl
		 << "p v -- Show if the value v is present in the list" << endl
		 << "n v -- Show the contents of the nth node." << endl
		 << "w   -- Write out the list" << endl
		 << "h   -- See this menu" << endl
		 << "q   -- Quit" << endl
		 << "Enter your choice: " << endl;
}

int main()
{
	List MenuList; //List the program operates on
	char Choice; //User's command letter choice
	int Value; //Input value from the user
	int Results; //Output results from functions that return an integer

	DisplayMenu(); //Show the menu the first time
	
	while(Choice != 'q') //If choice is q, stop loop
	{
		cout << ">>> "; //PS1=">>> "
		cin >> Choice;
		if(Choice == 'i' || Choice == 'd' || Choice == 'p' || Choice == 'n')
		{//if the choice requires an argument, then read it in
			cin >> Value;
		}
		
		switch(Choice) //Applies the correct action based on the user choice
		{
			case 'e':
				MenuList.EmptyList();
				cout << "The list has been emptied" << endl;
				break;
			case 'i':
				MenuList.Insert(Value);
				cout << Value << " has been inserted into the list" << endl;
				break;
			case 'd':
				MenuList.Delete(Value);
				cout << Value << " has been deleted from the list" << endl;
				break;
			case 'm':
				if(MenuList.IsEmpty())
				{
					cout << "The list is empty" << endl;
				}
				else
				{
					cout << "The list is not empty" << endl;
				}
				break;
			case 'l':
				Results = MenuList.GetLength();
				cout << "There are " << Results << " items in the list" << endl;
				break;
			case 'p':
				if(MenuList.IsPresent(Value))
				{
					cout << "The value " << Value << " is in the list" << endl;
				}
				else
				{
					cout << "The value " << Value << " is not in the list" << endl;
				}
				break;
			case 'n':
				Results = MenuList.GetNthNodeItem(Value);
				if(Results == -1) //Means that the node could not be found
				{
					cout << "Node number " << Value << " does not exist" << endl;
				}
				else
				{
					cout << "Node number " << Value << " has value " << Results << endl;
				}
				break;
			case 'w':
				cout << MenuList << endl; //Display menu list
				break;
			case 'h':
				DisplayMenu();
				break;
			case 'q':
				cout << "Exiting" << endl;
				break;
			default: //The user didn't type the right command
				cout << "You've entered an incorrect command. "
					 << "If you need help, type 'h'" << endl;
		}
	}
}
