//File: 		ListClass.h
//Name: 		Kevin Clark
//UserID:		cs301117
//Class:		CSCI301 section 1
//Due:			02/15/2018
//Project: 		#5
//Purpose:		Prototypes many functions to implement a simple
//				singly-linked list ADT

#ifndef LIST
#define LIST

#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;



class List
{
	typedef int ItemType; //Change int to something else to use for another item type

	private:
		struct Node
		{
			ItemType Item;
			Node* Next;
		};
		Node* First; //Pointer to first node
		Node* NewNode(ItemType ItemValue, Node* NextPtr); //Same as get_node, but named better
		
	public:
		List() //Constructor: creates empty list
		{
			First = NULL;
		}
		void operator=(List Source); //Copy Constructor
		void EmptyList(); //Deletes all nodes in a list and sets First = NULL;
		bool IsPresent(ItemType SearchItem) const; //Checks if a value is present in a list
		bool IsEmpty() const; //Checks if the list is empty
		void Insert(ItemType Entry); //Puts an item in a list
		void Delete(ItemType Target); //Delete target item from the list
		int GetLength() const; //Returns the length of a list
		void Merge(List List1, List List2); //Merges two lists
		ItemType GetNthNodeItem(int NodeNumber) const;
		
		//Allows use of the '<<' operator to send a list to an output stream
		friend ostream& operator<<(ostream& Output, List& OutList);
		~List() //Destructor
		{
			EmptyList(); //Deletes all nodes in a list and sets First = NULL;
		}
};
#endif